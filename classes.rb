require 'date'

def stringtodate(data) ##Transforma uma string no formato XX-YYY-Z em [X, YYY, ZZ].to_i()
    ymd = []
    temp = ''
    data.split('').each{|x| ##Separação do string por hífen
        if(x == '-')
            temp = temp.to_i()
            ymd.append(temp)
            temp = ''
        else
            temp += x
        end
    }
    temp = temp.to_i()
    ymd.append(temp)
    return ymd
end

class Usuario
    attr_accessor :email
    attr_accessor :nome
    attr_accessor :nascimento
    attr_accessor :online
    def initialize(email:,senha:,nome:,nascimento:)
      @email = email
      @senha = senha
      @nome = nome
      @nascimento = nascimento
      @online = false 
    end

    def idade  ##Determina idade através da data de Nascimento
        ymd = stringtodate(@nascimento) ##Função do início do script, string to [YYYY, MM, DD]
        nasc = Date.new(ymd[0], ymd[1], ymd[2]) ##Passa para a classe Date
        
        idade = (Date.today - nasc)/365.25 ##Cálculo da idade apartir da data atual

        return idade.to_i() ##Return um int ao invés de irrational
    end

    def logar(senha) ## Se input for igual senha, @online = True
        if(senha == @senha)
            @online = true
        end
    end

    def deslogar
        @online = false
    end
end

class Aluno < Usuario
    attr_accessor :matricula
    attr_accessor :periodo
    attr_accessor :curso
    attr_accessor :turmas
    def initialize(email:,senha:,nome:,nascimento:,matricula:,periodo:,curso:,turmas:)
        super(email: email,senha: senha,nome: nome,nascimento: nascimento)
        @matricula = matricula
        @periodo = periodo
        @curso = curso
        @turmas = turmas
    end
end 

class Professor < Usuario
    attr_accessor :matricula
    attr_accessor :salario
    attr_accessor :materias
    def initialize(email:, senha:, nome:, nascimento:, matricula:, salario:, materias:)
        super(email: email,senha: senha,nome: nome,nascimento: nascimento)
        @matricula = matricula
        @salario = salario
        @materias = materias
    end
    def adicionar_materia(materia)
        @materias.append(materia)
    end
    def adicionar_materias(materias)
        for x in materias
            @materias.append(x)
        end
    end
end


class Turma
    attr_accessor :nome
    attr_accessor :horario
    attr_accessor :dia_da_semana
    attr_accessor :inscritos
    attr_accessor :inscricao_aberta
    def initialize(nome:,horario:,dia_da_semana:,inscritos:)
        @nome = nome
      @horario = horario
      @dsemana = dia_da_semana
      @inscritos = inscritos
      @inscricao_aberta = false
    end
    def abrir_inscricao
        @inscricao_aberta = true
    end
    def fechar_inscricao
        @inscricao_aberta = false
    end
    def adicionar_aluno(aluno)
        @inscritos.append(aluno)
    end
end


class Materia
    attr_accessor :ementa
    attr_accessor :nome
    attr_accessor :professores
    def initialize(ementa:,nome:,professores:)
        @ementa = ementa
        @nome = nome
        @professores = professores
    end
    def adicionar_professores(prof)
        @professores.append(prof)
    end
end

####Exemplos####
aluno = Aluno.new(email:'a@a.com',senha:'123',nome:'Per',nascimento:'2009-2-4',matricula:'12.23.34',periodo: 0,curso: '',turmas:[])  

prof = Professor.new(email:'a@a.com',senha:'123',nome:'Jão',nascimento:'1989-3-6',matricula:'15.46.23',salario:"900.00",materias:['Cálculo','Algebra Linear'])

turma = Turma.new(nome:'Cálculo',horario:"11:00-13:00",dia_da_semana:'sab',inscritos:[])

calculo = Materia.new(ementa:'?',nome:"Cálculo",professores:[])
################